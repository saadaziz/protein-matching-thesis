/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gun2sh
 */
@Entity
@Table(name = "p_protein")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PProtein.findAll", query = "SELECT p FROM PProtein p"),
    @NamedQuery(name = "PProtein.findByPid", query = "SELECT p FROM PProtein p WHERE p.pid = :pid"),
    @NamedQuery(name = "PProtein.findByPdbid", query = "SELECT p FROM PProtein p WHERE p.pdbid = :pdbid"),
    @NamedQuery(name = "PProtein.findByScopsidOld", query = "SELECT p FROM PProtein p WHERE p.scopsid = :scopsid and p.pid<4898"),
    @NamedQuery(name = "PProtein.findByScopsid", query = "SELECT p FROM PProtein p WHERE p.scopsid = :scopsid"),
    @NamedQuery(name = "PProtein.findByName", query = "SELECT p FROM PProtein p WHERE p.name = :name"),
    @NamedQuery(name = "PProtein.findByDataset", query = "SELECT p FROM PProtein p WHERE p.dataset = :dataset")})
public class PProtein implements Serializable {
    @OneToMany(mappedBy = "pid1")
    private Collection<PEdgelist> pEdgelistCollection;
    @OneToMany(mappedBy = "pid2")
    private Collection<PEdgelist> pEdgelistCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pid")
    private Collection<PDetails> pDetailsCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pid")
    private Integer pid;
    @Basic(optional = false)
    @Column(name = "pdbid")
    private String pdbid;
    @Basic(optional = false)
    @Column(name = "scopsid")
    private String scopsid;
    @Column(name = "Name")
    private String name;
    @Lob
    @Column(name = "histogram")
    private String histogram;
    @Lob
    @Column(name = "histogram128")
    private String histogram128;
    @Column(name = "Dataset")
    private Integer dataset;

    public PProtein() {
    }

    public PProtein(Integer pid) {
        this.pid = pid;
    }

    public PProtein(Integer pid, String pdbid, String scopsid) {
        this.pid = pid;
        this.pdbid = pdbid;
        this.scopsid = scopsid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getPdbid() {
        return pdbid;
    }

    public void setPdbid(String pdbid) {
        this.pdbid = pdbid;
    }

    public String getScopsid() {
        return scopsid;
    }

    public void setScopsid(String scopsid) {
        this.scopsid = scopsid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHistogram() {
        return histogram;
    }

    public void setHistogram(String histogram) {
        this.histogram = histogram;
    }

    public String getHistogram128() {
        return histogram128;
    }

    public void setHistogram128(String histogram128) {
        this.histogram128 = histogram128;
    }

    public Integer getDataset() {
        return dataset;
    }

    public void setDataset(Integer dataset) {
        this.dataset = dataset;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pid != null ? pid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PProtein)) {
            return false;
        }
        PProtein other = (PProtein) object;
        if ((this.pid == null && other.pid != null) || (this.pid != null && !this.pid.equals(other.pid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.PProtein[ pid=" + pid + " ]";
    }

    @XmlTransient
    public Collection<PDetails> getPDetailsCollection() {
        return pDetailsCollection;
    }

    public void setPDetailsCollection(Collection<PDetails> pDetailsCollection) {
        this.pDetailsCollection = pDetailsCollection;
    }

    
    @XmlTransient
    public Collection<PEdgelist> getPEdgelistCollection() {
        return pEdgelistCollection;
    }

    public void setPEdgelistCollection(Collection<PEdgelist> pEdgelistCollection) {
        this.pEdgelistCollection = pEdgelistCollection;
    }

    @XmlTransient
    public Collection<PEdgelist> getPEdgelistCollection1() {
        return pEdgelistCollection1;
    }

    public void setPEdgelistCollection1(Collection<PEdgelist> pEdgelistCollection1) {
        this.pEdgelistCollection1 = pEdgelistCollection1;
    }
    
}
