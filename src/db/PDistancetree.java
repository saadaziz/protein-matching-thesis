/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gun2sh
 */
@Entity
@Table(name = "p_distancetree")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PDistancetree.findAll", query = "SELECT p FROM PDistancetree p"),
    @NamedQuery(name = "PDistancetree.findByDiffID", query = "SELECT p FROM PDistancetree p WHERE p.diffID = :diffID"),
    @NamedQuery(name = "PDistancetree.findByDistance", query = "SELECT p FROM PDistancetree p WHERE p.distance = :distance")})
public class PDistancetree implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "diffID")
    private Integer diffID;
    @Basic(optional = false)
    @Column(name = "distance")
    private int distance;
    @JoinColumn(name = "firstpid", referencedColumnName = "pid")
    @ManyToOne(optional = false)
    private PProtein firstpid;
    @JoinColumn(name = "secondpid", referencedColumnName = "pid")
    @ManyToOne(optional = false)
    private PProtein secondpid;

    public PDistancetree() {
    }

    public PDistancetree(Integer diffID) {
        this.diffID = diffID;
    }

    public PDistancetree(Integer diffID, int distance) {
        this.diffID = diffID;
        this.distance = distance;
    }

    public Integer getDiffID() {
        return diffID;
    }

    public void setDiffID(Integer diffID) {
        this.diffID = diffID;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public PProtein getFirstpid() {
        return firstpid;
    }

    public void setFirstpid(PProtein firstpid) {
        this.firstpid = firstpid;
    }

    public PProtein getSecondpid() {
        return secondpid;
    }

    public void setSecondpid(PProtein secondpid) {
        this.secondpid = secondpid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (diffID != null ? diffID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PDistancetree)) {
            return false;
        }
        PDistancetree other = (PDistancetree) object;
        if ((this.diffID == null && other.diffID != null) || (this.diffID != null && !this.diffID.equals(other.diffID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.PDistancetree[ diffID=" + diffID + " ]";
    }
    
}
