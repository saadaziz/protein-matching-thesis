/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author rezaul karim
 */
public class queryDB {

    ///"PCamat32.findAll"
    
    
    public List<PCamat32> getAllMat64s() {
        List<PCamat32>  res = new ArrayList<PCamat32>();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            res = em.createNamedQuery("PCamat32.findAll", PCamat32.class).getResultList();
            //res =  results;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }
    
    public PCamat32 getMat64(String scopid) {
        PCamat32 res = new PCamat32();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            List results = em.createNamedQuery("PCamat32.findByScopsid").setParameter("scopsid", scopid).getResultList();
            res = (PCamat32) results.get(0);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }

    public List<PProtein> getproteins() {
        List<PProtein> res = new ArrayList<PProtein>();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            res = em.createNamedQuery("PProtein.findAll", PProtein.class).getResultList();
            //res =  results;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }
    public List<PProtein> getproteinsbyDataset(int dataset) {
        List<PProtein> res = new ArrayList<PProtein>();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            res = em.createNamedQuery("PProtein.findByDataset", PProtein.class).setParameter("dataset", dataset).getResultList();
            //res =  results;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }
    public PProtein getproteinsOLD(String pdbid) {
        PProtein res = new PProtein();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            res = em.createNamedQuery("PProtein.findByScopsidOld", PProtein.class)
                    .setParameter("scopsid", pdbid).getResultList().get(0);
            //res =  results;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }
    public List<PProtein> getproteins(PProtein pr) {
        List<PProtein> res = new ArrayList<PProtein>();
        try {  ////////// //////////
            
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            res = em.createNamedQuery("PProtein.findByPidGreater", PProtein.class).setParameter("pid", pr).getResultList();
            //res =  results;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
        }

    public PProtein getprotein(int pr) {
        PProtein res = new PProtein();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            List results = em.createNamedQuery("PProtein.findByPid", PProtein.class).setParameter("pid", pr).getResultList();
            res = (PProtein) results.get(0);
            //res =  results;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }

    public List<PTree> gettree() {
        List<PTree> res = new ArrayList<PTree>();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            res = em.createNamedQuery("PTree.findAllOrderbyTID", PTree.class).getResultList();
            //res =  results;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }

    public PProtein getprotein(String scopid) {
        PProtein res = new PProtein();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            List results = em.createNamedQuery("PProtein.findByScopsid").setParameter("scopsid", scopid).getResultList();
            res = (PProtein) results.get(0);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }

    public PProtein gettreeByparent(String scopid) {
        PProtein res = new PProtein();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            List results = em.createNamedQuery("PProtein.findByScopsid").setParameter("scopsid", scopid).getResultList();
            res = (PProtein) results.get(0);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }
    public boolean  ifProteinexists(String scopid) {
        boolean res = false;
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            List results = em.createNamedQuery("PProtein.findByScopsid").setParameter("scopsid", scopid).getResultList();
            //PProtein res1 = (PProtein) results.get(0);
            if(!results.isEmpty())
                res=true;
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        //System.out.println((res)?"yes":"no");
        return res;
    }
    public PTree getTreenodeByName(String name) {
        PTree res = new PTree();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            List results = em.createNamedQuery("PTree.findByName").setParameter("name", name).getResultList();
            res = (PTree) results.get(0);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }
    public PDetails getDetailsByPid(PProtein name) {
        PDetails res = new PDetails();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            List results = em.createNamedQuery("PDetails.FindByPid").setParameter("pid", name.getPid()).getResultList();
            res = (PDetails) results.get(0);
        } catch (Exception ex) {
            System.out.println("Ex: "+ex.getMessage());
            return null;
        }
        return res;
    }
    public boolean  ifDetailsByPid(PProtein name) {
        boolean res = false;
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            List results = em.createNamedQuery("PDetails.FindByPid").setParameter("pid", name.getPid()).getResultList();
            if(!results.isEmpty())
                res=true;
        } catch (Exception ex) {
            System.out.println("Ex: "+ex.getMessage());
            return false;
        }
        return res;
    }
    public List<PTree> getallexceptroot(String name) {
        List<PTree> res = new ArrayList<PTree>();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            res = em.createNamedQuery("PTree.findAllExceptRoot", PTree.class).setParameter("name", name).getResultList();
            //res =  results;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }

    public List<PCamat> getallCamat64(PProtein pr) {
        List<PCamat> res = new ArrayList<PCamat>();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            res = em.createNamedQuery("PCamat.FindByPid", PCamat.class).setParameter("pid", pr).getResultList();
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }
    public boolean  ifDetailsExists(String findBySunid) {
        boolean res = false;
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            List<PDetails> res2 = em.createNamedQuery("PDetails.findBySunid", PDetails.class).setParameter("sunid", findBySunid).getResultList();
            res=(!res2.isEmpty())?true:false;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }
public List<PCamat32> getallCamat32(PProtein pr) {
        List<PCamat32> res = new ArrayList<PCamat32>();
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            res = em.createNamedQuery("PCamat32.FindByPid", PCamat32.class).setParameter("pid", pr).getResultList();
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return res;
    }
}
