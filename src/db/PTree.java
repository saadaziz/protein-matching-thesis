/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gun2sh
 */
@Entity
@Table(name = "p_tree")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PTree.findAll", query = "SELECT p FROM PTree p"),
    @NamedQuery(name = "PTree.findByTid", query = "SELECT p FROM PTree p WHERE p.tid = :tid"),
    @NamedQuery(name = "PTree.findByChildCount", query = "SELECT p FROM PTree p WHERE p.childCount = :childCount"),
    @NamedQuery(name = "PTree.findByDistance", query = "SELECT p FROM PTree p WHERE p.distance = :distance")})
public class PTree implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tid")
    private Integer tid;
    @Basic(optional = false)
    @Column(name = "child_count")
    private int childCount;
    @Basic(optional = false)
    @Column(name = "distance")
    private int distance;
    @OneToMany(mappedBy = "parentid")
    private Collection<PTree> pTreeCollection;
    @JoinColumn(name = "parentid", referencedColumnName = "tid")
    @ManyToOne
    private PTree parentid;
    @JoinColumn(name = "pid", referencedColumnName = "pid")
    @ManyToOne(optional = false)
    private PProtein pid;

    public PTree() {
    }

    public PTree(Integer tid) {
        this.tid = tid;
    }

    public PTree(Integer tid, int childCount, int distance) {
        this.tid = tid;
        this.childCount = childCount;
        this.distance = distance;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public int getChildCount() {
        return childCount;
    }

    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @XmlTransient
    public Collection<PTree> getPTreeCollection() {
        return pTreeCollection;
    }

    public void setPTreeCollection(Collection<PTree> pTreeCollection) {
        this.pTreeCollection = pTreeCollection;
    }

    public PTree getParentid() {
        return parentid;
    }

    public void setParentid(PTree parentid) {
        this.parentid = parentid;
    }

    public PProtein getPid() {
        return pid;
    }

    public void setPid(PProtein pid) {
        this.pid = pid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tid != null ? tid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PTree)) {
            return false;
        }
        PTree other = (PTree) object;
        if ((this.tid == null && other.tid != null) || (this.tid != null && !this.tid.equals(other.tid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.PTree[ tid=" + tid + " ]";
    }
    
}
