/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gun2sh
 */
@Entity
@Table(name = "p_edgelist")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PEdgelist.findAll", query = "SELECT p FROM PEdgelist p"),
    @NamedQuery(name = "PEdgelist.findByEdgeListid", query = "SELECT p FROM PEdgelist p WHERE p.edgeListid = :edgeListid"),
    @NamedQuery(name = "PEdgelist.findByDistance", query = "SELECT p FROM PEdgelist p WHERE p.distance = :distance"),
    @NamedQuery(name = "PEdgelist.findByMethod", query = "SELECT p FROM PEdgelist p WHERE p.method = :method")})
public class PEdgelist implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "edgeList_id")
    private Integer edgeListid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "distance")
    private Double distance;
    @Column(name = "method")
    private String method;
    @JoinColumn(name = "pid1", referencedColumnName = "pid")
    @ManyToOne
    private PProtein pid1;
    @JoinColumn(name = "pid2", referencedColumnName = "pid")
    @ManyToOne
    private PProtein pid2;

    public PEdgelist() {
    }

    public PEdgelist(Integer edgeListid) {
        this.edgeListid = edgeListid;
    }

    public Integer getEdgeListid() {
        return edgeListid;
    }

    public void setEdgeListid(Integer edgeListid) {
        this.edgeListid = edgeListid;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public PProtein getPid1() {
        return pid1;
    }

    public void setPid1(PProtein pid1) {
        this.pid1 = pid1;
    }

    public PProtein getPid2() {
        return pid2;
    }

    public void setPid2(PProtein pid2) {
        this.pid2 = pid2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (edgeListid != null ? edgeListid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PEdgelist)) {
            return false;
        }
        PEdgelist other = (PEdgelist) object;
        if ((this.edgeListid == null && other.edgeListid != null) || (this.edgeListid != null && !this.edgeListid.equals(other.edgeListid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.PEdgelist[ edgeListid=" + edgeListid + " ]";
    }
    
}
