/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author rezaul karim
 */
public class insertDB {

    public void insertProtein(PProtein p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void insertEdgelist(PEdgelist p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    public PProtein insertProteinRet(PProtein p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return p;
    }
    public void updateProtein(PProtein p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.createQuery("update PProtein p set p.dataset=1 where p.pid="+p.getPid()).executeUpdate();
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    public PTree insertTree(PTree p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return p;
    }
    public PDetails insertDetails(PDetails p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return p;
    }
    public PDistancetree insertdistanceTree(PDistancetree p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return p;
    }
    public void insertcamat(PCamat p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void insertcamat32(PCamat32 p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void insertcamat16(PCamat16 p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void insertcamat8(PCamat8 p) {
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("protein_insertionPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction entr = em.getTransaction();
            entr.begin();
            em.persist(p);
            entr.commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
