/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 *
 * @author Gun2sh
 */
public class StringSort3token1key {

    public static void main(String[] args) {

        Scanner sc = null;
        PrintWriter pwEDwithangle = null;
        File inf;
        //String indirname="E:\\Thesis\\scopdataExperiment\\matdata\\gray128";
        //E:\Thesis\19-12-13-protein\comog1norm32level_gray32
        //String indirname="E:\\Thesis\\scopdataExperiment\\matdata\\mydistance_files_all\\New folder";
        //String indirname="M:\\4-2\\protein\\all sorted for test\\comog1norm64level_gray128";
        String indirname = "M:\\4-2\\protein\\mat\\forSortMASASW";
        //String outdirname="E:\\Thesis\\scopdataExperiment\\codes in matlab\\comog";
        //String outdirname="E:\\Thesis\\19-12-13-protein\\comog1norm32level_gray32_sorted";//E:\Thesis\scopdataExperiment\matdata\gray64\distancefiles\
        //E:\Thesis\scopdataExperiment\codes in matlab\distance codes\distancefiles
        String outdirname = "M:\\4-2\\protein\\mat\\sortedMASASW";

        // String outdirname="E:\\Thesis\\scopdataExperiment\\matdata\\mydistance_files_all\\sorted\\New folder";
        File inDir = new File(indirname);
        File[] infiles = inDir.listFiles();
        System.out.println(inDir.isDirectory());
        System.out.println(infiles.length);
        for (int i = 0; i < infiles.length; i++)// {
        {
            //File inf=new File("E:\\Thesis\\scopdataExperiment\\matdata\\gray64\\distancefiles\\comog1norm_d1j3ha_.ent");
            inf = infiles[i];
            if (!(inf.getName().endsWith(".ent"))) {
                continue;
            }

            System.out.println(inf.getName().toString());

            File outfEDCS = new File(outdirname + "\\" + inf.getName());

            try {
                sc = new Scanner(inf);
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
            try {
                pwEDwithangle = new PrintWriter(outfEDCS);
            } catch (FileNotFoundException ex) {
            }
            String line;
            Map<Integer, String> linesmapEDwithangle = null;
            linesmapEDwithangle = new HashMap<Integer, String>();
            String str = null;
            sc.nextLine();
            while (sc.hasNext()) {
                line = sc.nextLine();
                StringTokenizer strtok = new StringTokenizer(line, "\t");
                int toklen = strtok.countTokens();
                //System.out.println("line: "+line);
                // System.out.println("toklen: "+toklen);
                if (toklen == 3) {
                    //System.out.println("");
                    String[] tokens = new String[toklen];
                    for (int j = 0; j < toklen; j++) {
                        tokens[j] = strtok.nextToken();
                    }

                    String oldstr = null;
                    String newstr = null;

                    int d = (int) Double.parseDouble(tokens[2]);
                    //System.out.println("d: "+d);
                    oldstr = linesmapEDwithangle.get(d);
                    if (oldstr == null) {
                        newstr = tokens[0] + "\t   " + tokens[1] + "\t   " + d;
                    } else {
                        newstr = oldstr.concat(" :").concat(tokens[0] + "\t   " + tokens[1] + "\t   " + d);
                    }

                    linesmapEDwithangle.put(d, newstr);


                } else { //System.out.println(str);}
                }

            }
            //Collections.sort(lines);

            Map<Integer, String> lines_treemapEDwithangle = null;
            lines_treemapEDwithangle = new TreeMap<Integer, String>(Collections.reverseOrder());
            for (Map.Entry en : linesmapEDwithangle.entrySet()) {
                lines_treemapEDwithangle.put(Integer.parseInt( en.getKey().toString()), en.getValue().toString());

            }
            System.out.println("treeMapSize: " + lines_treemapEDwithangle.size());
            for (Map.Entry en : lines_treemapEDwithangle.entrySet()) {
                String ss = en.getValue().toString();

                StringTokenizer strTok = new StringTokenizer(ss, ":");
                while (strTok.hasMoreTokens()) {

                    pwEDwithangle.println(strTok.nextToken().toString());
                    pwEDwithangle.flush();
                }
                pwEDwithangle.flush();
            }
            pwEDwithangle.checkError();
            pwEDwithangle.close();
        }
    }
}