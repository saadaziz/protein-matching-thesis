/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import db.PCamat;
import java.io.File;
import java.util.ArrayList;
import db.PProtein;
import db.insertDB;
import db.queryDB;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author rezaul karim
 */
public class Protein_insertion {

    /**
     * @param args the command line arguments
     */
    public String get_hist128(File hist) throws IOException {
        Scanner sc = new Scanner(hist);
        String res = "";
        while (sc.hasNext()) {
            String line = sc.nextLine();
            StringTokenizer strTok = new StringTokenizer(line, " ");
            int dim = strTok.countTokens();
            for (int i = 0; i < dim; i++) {
                if (i % 2 != 0) {
                    res += strTok.nextToken() + " ";
                } else {
                    strTok.nextToken();
                }

            }
        }
        return res;

    }

    public void insert_proteintable() throws IOException {
        insertDB idb = new insertDB();

        File hist128dir = new File("E:\\Thesis\\data\\camat128histq255");
        File pdbmap = new File("E:\\Thesis\\data\\summary\\pdbmap.ent");
        File camathistq2dir = new File("E:\\Thesis\\data\\camathistq2");
        File[] hist128file = hist128dir.listFiles();
        ArrayList<PProtein> pproteins = new ArrayList<PProtein>();
        ArrayList<String> scopid = new ArrayList<String>();
        ArrayList<String> pdbid = new ArrayList<String>();
        Scanner sc = new Scanner(pdbmap);
        while (sc.hasNext()) {
            String line = sc.nextLine();
            StringTokenizer strTok = new StringTokenizer(line, " ");
            int dim = strTok.countTokens();
            for (int i = 0; i < dim; i++) {
                if (i % 2 == 0) {
                    scopid.add(strTok.nextToken());
                } else {
                    pdbid.add(strTok.nextToken());
                }

            }
//                System.out.println(strTok.nextToken());

        }
        for (File s : hist128file) {
            File histnormal = new File(camathistq2dir.toString() + "\\" + s.getName());
            String hist128 = new Protein_insertion().get_hist128(s);
            String hist = new Protein_insertion().get_hist128(histnormal);
            //System.out.println(hist);
            PProtein p = new PProtein();
            p.setHistogram(hist);
            p.setHistogram128(hist128);
            String scopidstr = s.getName().substring(0, s.getName().indexOf(".ent"));
            p.setScopsid(scopidstr);
            p.setPdbid(pdbid.get(scopid.indexOf(scopidstr)));
            pproteins.add(p);
            System.out.println(p.getScopsid() + " " + p.getPdbid());
            idb.insertProtein(p);
        }
    }

    public static void main(String[] args) throws IOException {
        insertDB idb = new insertDB();
        File camat64mat = new File("E:\\Thesis\\data\\camat64");
        File hist128dir = new File("E:\\Thesis\\data\\camat128histq255");
        File[] camatarr = camat64mat.listFiles();

        ArrayList<File> camat64files = new ArrayList<File>(Arrays.asList(camatarr));

        File[] hist128file = hist128dir.listFiles();
        //ArrayList<File> mat64files = new ArrayList<File>(Arrays.asList(hist128file));

        for (File s : hist128file) {
            File fl = new File("E:\\Thesis\\data\\camat64\\" + s.getName());
            //System.out.println(camat64files.get(camat64files.indexOf(fl)).getName());
            File histnormal = camat64files.get(camat64files.indexOf(fl));
            Scanner sc = new Scanner(histnormal);
            System.out.println(s.getName());
            PProtein pr = new queryDB().getprotein(s.getName().substring(0, s.getName().indexOf(".ent")));
            //System.out.println(pr.getScopsid() + " " + pr.getPdbid());
            PCamat pcmat = new PCamat();
            pcmat.setPid(pr);
            pcmat.setMatDim(64);
            int rowno = -1;
            if(pr.getPid()<4253) continue;
            while (sc.hasNext()) {
                rowno++;
                String line = sc.nextLine();
                StringTokenizer strTok = new StringTokenizer(line, " ");
                int dim = strTok.countTokens();
                //System.out.println(dim);
                int colno = 0;
                for (int i = 1; i <= dim; i++) {
                    String val = strTok.nextToken();
                    if (i % 8 == 0) {
                        pcmat.setCol8(Integer.parseInt(val));
                        //rowno = i / dim;
                        pcmat.setRowNo(rowno);
                        pcmat.setColNo(colno);
                        colno++;

                        // System.out.println(pcmat.getCol1()+" "+pcmat.getCol2()+" "+pcmat.getCol3()+" rowno "
                        //       +pcmat.getColNo());
                        new insertDB().insertcamat(pcmat);
                    } else if (i % 8 == 1) {
                        pcmat.setCol1(Integer.parseInt(val));
                    } else if (i % 8 == 2) {
                        pcmat.setCol2(Integer.parseInt(val));
                    } else if (i % 8 == 3) {
                        pcmat.setCol3(Integer.parseInt(val));
                    } else if (i % 8 == 4) {
                        pcmat.setCol4(Integer.parseInt(val));
                    } else if (i % 8 == 5) {
                        pcmat.setCol5(Integer.parseInt(val));
                    } else if (i % 8 == 6) {
                        pcmat.setCol6(Integer.parseInt(val));
                    } else if (i % 8 == 7) {
                        pcmat.setCol7(Integer.parseInt(val));
                    }

                }
            }
        }
        //System.out.println(s+ "  "+ pdbid.get(scopid.indexOf(s)) );
//        for(File f : hist128file)
//        {
//            
//        }
    }
}
