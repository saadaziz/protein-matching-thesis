/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

//import Catalano.Imaging.Concurrent.Filters.OtsuThreshold;
//import Catalano.Imaging.FastBitmap;
//import Catalano.Imaging.Filters.Grayscale;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Gun2sh
 */
public class readImage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        
        BufferedImage image = ImageIO.read(new File("M:\\4-2\\protein\\gray64\\d1a0da_.jpg"));
        byte[][] pixels = new byte[image.getWidth()][];

        for (int x = 0; x < image.getWidth(); x++) {
            pixels[x] = new byte[image.getHeight()];

            for (int y = 0; y < image.getHeight(); y++) {
                pixels[x][y] = (byte) (image.getRGB(x, y) == 0xFFFFFFFF ? 0 : 1);
                 System.out.print(pixels[x][y]);
            }
            System.out.println("");
        }
        
        byte BLACK = (byte)0, WHITE = (byte)255;
    byte[] map = {BLACK, WHITE};
    IndexColorModel icm = new IndexColorModel(1, map.length, map, map, map);

    // create checkered data
    int w = image.getWidth(),h = image.getHeight();
    WritableRaster raster = icm.createCompatibleWritableRaster(w, h);
    
    //int[] data = new int[w*h];
    for(int i=0; i<w; i++)
        for(int j=0; j<h; j++)
            raster.setSample(i,j,0,pixels[i][j]);

    // create image from color model and data
    
    //raster.setPixels(0, 0, w, h, data);
    BufferedImage bi = new BufferedImage(w,h,BufferedImage.TYPE_BYTE_GRAY);
    bi.setData(raster);

    // output to a file
    ImageIO.write(bi, "jpg", new File("C:\\Users\\Gun2sh\\Desktop\\test.jpg"));
        /*FastBitmap fb = new FastBitmap("M:\\4-2\\protein\\gray64\\d1a0da_.jpg");
        fb.toGrayscale();

        OtsuThreshold otsu = new OtsuThreshold();
        otsu.applyInPlace(fb);

        int[][] image = new int[fb.getHeight()][fb.getWidth()];
        fb.toArrayGray(image);
        for(int i=0;i<fb.getHeight();i++)
        {
            for(int j=0;j<fb.getHeight();j++)
                System.out.print(image[i][j]);
        }*/
    }
}
