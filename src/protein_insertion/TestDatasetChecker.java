/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rezaul karim
 */
public class TestDatasetChecker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        File test1=new File("E:\\Thesis\\our work\\Georgina Mirceva\\dataset1.txt");
    
        File test_1=new File("E:\\Thesis\\our work\\Georgina Mirceva\\dataset_1.txt");
        PrintWriter pw=null;
        try {
             pw=new PrintWriter(test_1);
        } catch (FileNotFoundException ex) {
           // Logger.getLogger(TestDatasetChecker.class.getName()).log(Level.SEVERE, null, ex);
        
            System.out.println("output file error");
        }
        
        int count=0;
        Scanner sc=null;
        try {
            sc= new Scanner(test1);
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(TestDatasetChecker.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("File error");
        }
        
    
        String s=null;
        StringTokenizer strTok=null;
        while(sc.hasNext()){
        
        s=sc.nextLine();
        if(s.length()<7){continue;}
        
        strTok= new StringTokenizer(s, " ");
        String tk=null;
        if(strTok.countTokens()<2){continue;}
        while(strTok.hasMoreTokens()){
        tk=strTok.nextToken().toString();
        
        
        
        if(tk.equalsIgnoreCase("test")){
            System.out.println("training data: "+count);
            count=0;
        }
        
        if(tk.equalsIgnoreCase("training")){
            System.out.println("test data: "+count);
            count=0;
        }
        
        if(tk.length()==7 && tk.startsWith("d")){
            count++; 
            pw.write(tk.toString());
            pw.flush();
            pw.write("\t");
        }
        
        
        }
        
        
        
        
        if(s.length()>0){pw.println();pw.flush();}
        
        
        
        
        }
        pw.close();
        System.out.println("Number of chains: "+"\t"+count);
        
    
    }
}
