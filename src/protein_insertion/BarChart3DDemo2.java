/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPosition;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.CategoryLabelWidthType;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.text.TextBlockAnchor;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RefineryUtilities;
import org.jfree.ui.TextAnchor;
import org.jfree.util.Log;
import org.jfree.util.PrintStreamLogTarget;

/**
 * A simple demonstration application showing how to create a vertical bar chart.
 *
 */
public class BarChart3DDemo2 extends ApplicationFrame {

    // ****************************************************************************
    // * JFREECHART DEVELOPER GUIDE                                               *
    // * The JFreeChart Developer Guide, written by David Gilbert, is available   *
    // * to purchase from Object Refinery Limited:                                *
    // *                                                                          *
    // * http://www.object-refinery.com/jfreechart/guide.html                     *
    // *                                                                          *
    // * Sales are used to provide funding for the JFreeChart project - please    * 
    // * support us so that we can continue developing free software.             *
    // ****************************************************************************
    
    /**
     * Creates a new demo.
     *
     * @param title  the frame title.
     */
    public BarChart3DDemo2(final String title,DefaultCategoryDataset dst) {

        super(title);
        
        // create the chart...
        //final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        final DefaultCategoryDataset dataset =dst;
       /* dataset.addValue(23.0, "Series 1", "10");
        dataset.addValue(13.0, "Series 1", "20");
        dataset.addValue(7.0, "Series 1", "30");
        dataset.addValue(5.0, "Series 1", "40");*/
        final JFreeChart chart = createChart(dataset,title);
        
        // add the chart to a panel...
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1000, 500));
        setContentPane(chartPanel);

    }

    /**
     * Creates a chart.
     * 
     * @param dataset  the dataset.
     * 
     * @return The chart.
     */
    private JFreeChart createChart(final CategoryDataset dataset,String which) {
        
        final JFreeChart chart = ChartFactory.createBarChart3D(
            "Average "+which+" match for top 100 results",       // chart title
            "number of top results",                  // domain axis label
            "% of "+which+" match with query protein",                     // range axis label
            dataset,                     // data
            PlotOrientation.VERTICAL,  // orientation
            true,                        // include legend
            true,                        // tooltips
            false                        // urls
        );

        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setForegroundAlpha(1.0f);

        // left align the category labels...
        final CategoryAxis axis = plot.getDomainAxis();
        final CategoryLabelPositions p = axis.getCategoryLabelPositions();
        //axis.setUpperMargin(100);
        final CategoryLabelPosition left = new CategoryLabelPosition(
            RectangleAnchor.LEFT, TextBlockAnchor.CENTER_LEFT, 
            TextAnchor.CENTER_LEFT, 0.0,
            CategoryLabelWidthType.RANGE, 0.30f
        );
        axis.setCategoryLabelPositions(CategoryLabelPositions.replaceLeftPosition(p, left));
        
        return chart;        
    
    }
    
    /**
     * Starting point for the demonstration application.
     *
     * @param args  ignored.
     */
    public static void main(final String[] args) {
        //int width= ;
        /*for(String str : args)
            System.out.println(str);*/
        System.out.println("Length"+args.length);
        String[] tmpDataset = new String[10];
        String[] tmpArgs = new String[args.length/3];
        //0,3,6,9
        for(int i=0,j=0;i<tmpArgs.length;j+=3,i++)
        {
            tmpArgs[i]=args[j];
            //System.out.println(tmpArgs[i]);
        }
        /*System.out.println(tmpArgs.length);
        System.out.println("tmp length "+tmpDataset.length);*/
        Log.getInstance().addTarget(new PrintStreamLogTarget());
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        double tot= 10;
        for(int i=0;i<tmpDataset.length;i+=1)
        {
            double val= 0;
            //String name="";
            for(int j=i;j<tmpArgs.length;j+=args.length/30){
                val+=Double.parseDouble( tmpArgs[j]);
                //System.out.println("val: "+Double.parseDouble( tmpArgs[j]));
                
            }
            //for(int j=i;j<tmpArgs.length;j+=args.length/30)
            
            System.out.println((double)(100*val/(tot*(args.length/30)))+ " tot "+tot );
            dataset.addValue((double)(100*val/(tot*(args.length/30))), "results for bin size 16", String.valueOf( tot));
            tot+=10;
        }
        final BarChart3DDemo2 demo = new BarChart3DDemo2("Average fold match for top 100 results",dataset);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);

    }

}

