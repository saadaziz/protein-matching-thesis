/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import db.PDetails;
import db.PProtein;
import db.insertDB;
import db.queryDB;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Gun2sh
 */
public class parseScop {

    /**
     * @param args the command line arguments
     */
    public static PDetails fparsescop(String sunid, String scopid) {
        try {
            Document doc = Jsoup.connect("http://scop.berkeley.edu/sunid=" + sunid).timeout(10 * 1000).get();
            Elements descboxs = doc.getElementsByClass("descbox");
            int i = 0;
            String Class1 = "", fold = "", superf = "", family = "", protein = "", species = "";
            for (Element es : descboxs) {
                String text = es.select("a[href]").first().text().trim();
                if (i == 0) {
                    Class1 = text.substring(2, text.length()).replace(":", " ").trim();
                } else if (i == 1) {
                    fold = text.substring(4, text.length()).replace(":", " ").trim();
                } else if (i == 2) {
                    superf = text.substring(6, text.length()).replace(":", " ").trim();
                } else if (i == 3) {
                    family = text.substring(8, text.length()).replace(":", " ").trim();
                } else if (i == 4) {
                    protein = text.trim();
                } else if (i == 5) {
                    species = text.trim();
                } else {
                    continue;
                }

                i++;
            }
            System.out.println(Class1 + "," + fold + "," + superf + "," + family + "," + protein + "," + species);
            if (Class1 != "") {
                queryDB qdb = new queryDB();
                insertDB idb = new insertDB();
                PProtein pr = qdb.getprotein(scopid);
                PDetails pdetails = new PDetails();
                pdetails.setClass1(Class1);
                pdetails.setFold(fold);
                pdetails.setFamily(family);
                pdetails.setSuperfamily(superf);
                pdetails.setProtein(protein);
                pdetails.setSpecies(species);
                pdetails.setPid(pr);
                pdetails.setSunid(sunid);
                if (Class1 != "") {
                    pdetails = idb.insertDetails(pdetails);
                    return  pdetails;
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            fparsescop(sunid, scopid);
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        File fl = new File("M:\\4-2\\protein\\sunid map all\\allsunid.txt");
        //File fldataset = new File("M:\\4-2\\protein\\Georgina Mirceva\\dataset1.txt");
        Scanner sc = new Scanner(fl);
        //sc = new Scanner(fl);
        int i = 1;
        insertDB idb = new insertDB();
        queryDB qdb = new queryDB();
        List<PProtein> lstproList = qdb.getproteinsbyDataset(1);
        //List<PProtein> lstproList2 = qdb.getproteinsbyDataset(0);
        ArrayList<String> lstscop = new ArrayList<String>();
        for (PProtein pr : lstproList) {
            lstscop.add(pr.getScopsid());
        }
       /* for (PProtein pr : lstproList2) {
            lstscop.add(pr.getScopsid());
        }*/
        boolean flg = false;
        while (sc.hasNext()) {
            String line = sc.nextLine();
            StringTokenizer strTok = new StringTokenizer(line, "\n\r");
            int dim = strTok.countTokens();
            for (int j = 0; j < dim; j++) {
                StringTokenizer strtokspace = new StringTokenizer(strTok.nextToken(), "\t");
                if (strtokspace.countTokens() != 0) {
                    String scopid = strtokspace.nextToken().trim();
                    String sunid = strtokspace.nextToken().trim();
                    System.out.println("Scop:" + scopid + " sunid:" + sunid);
                    //if (lstscopq.contains(scopid)) {
                    if(qdb.ifProteinexists(scopid))
                    {  
                        if(!qdb.ifDetailsExists(sunid))
                            fparsescop(sunid, scopid);
                    }
                    else {
                        //System.out.println("here");
                        PProtein newprotein = new PProtein();
                        newprotein.setScopsid(scopid);
                        newprotein.setPdbid(scopid.substring(1, 5));
                        newprotein.setDataset(0);
                        idb.insertProtein(newprotein);
                        fparsescop(sunid, scopid);
                    }
                }
            }
        }
//        while (sc.hasNext()) {
//            String line = sc.nextLine();
//                StringTokenizer strTok = new StringTokenizer(line, " ");
//                int dim = strTok.countTokens();
//            //System.out.println(dim);
//                for (int j=0;j<dim;j++) {
//                    String scopid= strTok.nextToken();
//                if(scopid.startsWith("d") )
//                {        
//                    System.out.println("Count "+i+": "+scopid+" pdb: "+scopid.substring(1, 5));
//                    i++;
//                    PProtein pr = qdb.getproteinsOLD(scopid);
//                    if(pr.getScopsid()!=null)
//                    {
//                        //pr.setDataset(1);
//                        System.out.println("Protein "+pr.getScopsid());
//                        idb.updateProtein(pr);
//                    }
////                    PProtein pr = new PProtein();
////                    pr.setDataset(1);
////                    pr.setPdbid(scopid.substring(1, 5));
////                    pr.setScopsid(scopid);
////                    idb.insertProtein(pr);
//                }
//            }
//        }



    }
}
