/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import db.PCamat32;
import db.PProtein;
import db.insertDB;
import db.queryDB;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author rezaul karim
 */
public class insert32 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
         insertDB idb = new insertDB();
        File camat64mat = new File("E:\\Thesis\\data\\camat32");
        File hist128dir = new File("E:\\Thesis\\data\\camat128histq255");
        File[] camatarr = camat64mat.listFiles();

        ArrayList<File> camat64files = new ArrayList<File>(Arrays.asList(camatarr));

        File[] hist128file = hist128dir.listFiles();
        //ArrayList<File> mat64files = new ArrayList<File>(Arrays.asList(hist128file));

        for (File s : hist128file) {
            File fl = new File("E:\\Thesis\\data\\camat32\\" + s.getName());
            //System.out.println(camat64files.get(camat64files.indexOf(fl)).getName());
            File histnormal = camat64files.get(camat64files.indexOf(fl));
            Scanner sc = new Scanner(histnormal);
            System.out.println(s.getName());
            PProtein pr = new queryDB().getprotein(s.getName().substring(0, s.getName().indexOf(".ent")));
            //System.out.println(pr.getScopsid() + " " + pr.getPdbid());
            PCamat32 pcmat = new PCamat32();
            pcmat.setPid(pr);
            
            int rowno = -1;
            //if(pr.getPid()<255) continue;
            while (sc.hasNext()) {
                rowno++;
                String line = sc.nextLine();
                StringTokenizer strTok = new StringTokenizer(line, " ");
                int dim = strTok.countTokens();
                //System.out.println(dim);
                int colno = 0;
                for (int i = 1; i <= dim; i++) {
                    String val = strTok.nextToken();
                    if (i % 8 == 0) {
                        pcmat.setCol8(Integer.parseInt(val));
                        //rowno = i / dim;
                        pcmat.setRowNo(rowno);
                        pcmat.setColNo(colno);
                        colno++;

                        // System.out.println(pcmat.getCol1()+" "+pcmat.getCol2()+" "+pcmat.getCol3()+" rowno "
                           //    +pcmat.getColNo());
                        new insertDB().insertcamat32(pcmat);
                    } else if (i % 8 == 1) {
                        pcmat.setCol1(Integer.parseInt(val));
                    } else if (i % 8 == 2) {
                        pcmat.setCol2(Integer.parseInt(val));
                    } else if (i % 8 == 3) {
                        pcmat.setCol3(Integer.parseInt(val));
                    } else if (i % 8 == 4) {
                        pcmat.setCol4(Integer.parseInt(val));
                    } else if (i % 8 == 5) {
                        pcmat.setCol5(Integer.parseInt(val));
                    } else if (i % 8 == 6) {
                        pcmat.setCol6(Integer.parseInt(val));
                    } else if (i % 8 == 7) {
                        pcmat.setCol7(Integer.parseInt(val));
                    }

                }
            }
        }
    }
}
