package protein_insertion;

import db.PCamat;
import db.PCamat32;
import db.PProtein;
import db.queryDB;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class NewDistanceTest {

    public static void printmat(int[][] mat) {
        System.out.println("Printing");
        for (int k = 0; k < 32; k++) {
            for (int j = 0; j < 32; j++) {
                System.out.print(mat[k][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] makematrix64(PProtein mainprotein) {
        queryDB qdb = new queryDB();

        List<PCamat> lstcamat = qdb.getallCamat64(mainprotein);
        int[][] mat64mainpr = new int[64][64];
        ///System.out.println("Protein matrix for: " + mainprotein.getPid());
        int i = 0;
        int rownum = 0;
        for (PCamat cmtrows : lstcamat) {
            if (rownum != cmtrows.getRowNo()) {
                i = 0;
            }
            rownum = cmtrows.getRowNo();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol1();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol2();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol3();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol4();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol5();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol6();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol7();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol8();
        }
        return mat64mainpr;
    }
    
    
    public static int[][] makematrix32(PProtein mainprotein) {
        queryDB qdb = new queryDB();

        List<PCamat32> lstcamat = qdb.getallCamat32(mainprotein);
        int[][] mat32mainpr = new int[32][32];
        ///System.out.println("Protein matrix for: " + mainprotein.getPid());
        int i = 0;
        int rownum = 0;
        for (PCamat32 cmtrows : lstcamat) {
            if (rownum != cmtrows.getRowNo()) {
                i = 0;
            }
            rownum = cmtrows.getRowNo();
            mat32mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol1();
            mat32mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol2();
            mat32mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol3();
            mat32mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol4();
          
        }
        return mat32mainpr;
    }
    
    
//        double[][] hungtest = {{10,19,8,15},
//        {10 ,18,7,17},
//        {13,16,9,14},
//        {12,19,8,18},
//        {14,17,10,19}};
//        HungarianAlgoritm ha = new HungarianAlgoritm(hungtest);
//        int[] res = ha.execute();
//        for(int u=0;u<res.length;u++)
//            System.out.println(res[u]);

    public static double calcRms(int[] data, boolean[] mask) {

        int i, j;
        for (i = 0, j = 0; i < data.length; i++) {
            if (mask[i]) {
                j++;
            }
            //           System.out.println(data[i]+ " "+ mask[i]);
        }
        int newData[] = new int[j];
        double res = 0, n = 0;
        for (i = 0, j = 0; i < data.length; i++) {
            if (mask[i]) {
             //   System.out.print(data[i] + " ");
                newData[j++] = data[i];
            }
        }
       // System.out.println("");

        for (i = 0; i < newData.length; i++) {
            j = 1;
            if ((i + 1) == newData.length) {
                res += j * j;
                n++;
               // System.out.println("End position:" + i + "\tlen:" + j);
                break;
            } else if (newData[i] + 1 == newData[i + 1]) {

                for (; (i + 1) < newData.length; i++) {
                    if (newData[i] + 1 == newData[i + 1]) {
                        j++;
                       
                    } else {
               // System.out.println("End position:" + i + "\tlen:" + j);
                
                        break;
                    }
                }
                //i--;
               // System.out.println("End position:" + i + "\tlen:" + j+"\t increasing");

                n++;
                res += j * j;
            } else if (newData[i] - 1 == newData[i + 1]) {
                j = 1;

                for (; (i + 1) < newData.length; i++) {
                    if (newData[i] - 1 == newData[i + 1]) {
                        j++;
                    } else {
                        break;
                    }
                }

                //i--;
                // System.out.println("End position:" + i + "\tlen:" + j+"\t decresing");
                
                n++;
                res += j * j;
            } else {

               // System.out.println("End position:" + i + "\tlen:" + j+"\t single");
                
                res += j * j;
                n++;
            }
            
            // System.out.print(j+" ");

        }
        //System.out.println("");
       // System.out.println("number of Run:\t" + n + "\tSquared length sum:\t" + res);
        
        return res;

    }

    public static void main(String[] args) {

        //String outdir="E:\\Thesis\\codes\\protein_insertion\\output\\";
        
        File out = new File("D:\\out_dad.txt");
        PrintWriter dpw = null;
        try {
            dpw = new PrintWriter(out);
        } catch (FileNotFoundException ex) {
            System.out.println("OUTPUT FILE ERROR");
        }
        
        String mainProteinScopId=null;
        String comparedProteinScopId=null;
        int dim = 32;
        queryDB qdb = new queryDB();
        List<PProtein> allprotein = qdb.getproteins();
         for (PProtein mainprotein : allprotein) 
        {
            
            if(!mainprotein.getScopsid().equalsIgnoreCase("d1bhga1"))continue;
            
            Date begin = new Date();
       //     PProtein mainprotein = qdb.getprotein("d1bhga1");
            mainProteinScopId=mainprotein.getScopsid();
            
            System.out.println(mainprotein.getScopsid().toString());
            int[][] mat32mainProtein = makematrix32(mainprotein);
            
            //int[][][] patternGradMats = ProteinGradMats(mat64mainpr);
            
            int[][][] patternGradMats = ProteinGradMats(mat32mainProtein,dim);
            
           // Map<Integer, String> patternMaph = new HashMap<Integer, String>();
           // Map<Integer, String> patternMapv = new HashMap<Integer, String>();
            Map<Integer, String> patternMapd = new HashMap<Integer, String>();
            Map<Integer, String> patternMapad = new HashMap<Integer, String>();
           
            for (int i = 0; i < dim; i++) {
                for (int j = 0; j < dim; j++) {
                    String oldstr = null;
                    String newstr = null;
                   /*
                    oldstr = patternMaph.get(patternGradMats[0][i][j]);
                    if (oldstr == null) {
                        newstr = "" + i + ":" + j + "";
                    } else {
                        newstr = oldstr + ":" + i + ":" + j;
                    }
                    patternMaph.remove(patternGradMats[0][i][j]);
                    patternMaph.put(patternGradMats[0][i][j], newstr);
                    oldstr = null;
                    newstr = null;
                    oldstr = patternMapv.get(patternGradMats[1][i][j]);
                    if (oldstr == null) {
                        newstr = "" + i + ":" + j + "";
                    } else {
                        newstr = oldstr + ":" + i + ":" + j;
                    }
                    patternMapv.remove(patternGradMats[1][i][j]);
                    patternMapv.put(patternGradMats[1][i][j], newstr);
                    */ 
                    oldstr = null;
                    newstr = null;
                    oldstr = patternMapd.get(patternGradMats[2][i][j]);
                    if (oldstr == null) {
                        newstr = "" + i + ":" + j + "";
                    } else {
                        newstr = oldstr + ":" + i + ":" + j;
                    }
                    patternMapd.remove(patternGradMats[2][i][j]);
                    patternMapd.put(patternGradMats[2][i][j], newstr);

                    oldstr = null;
                    newstr = null;
                    oldstr = patternMapad.get(patternGradMats[3][i][j]);
                    if (oldstr == null) {
                        newstr = "" + i + ":" + j + "";
                    } else {
                        newstr = oldstr + ":" + i + ":" + j;
                    }
                    patternMapad.remove(patternGradMats[3][i][j]);
                    patternMapad.put(patternGradMats[3][i][j], newstr);
                }
            }

            Map<Integer, String> d_hashsmap = new HashMap<Integer, String>();

             for (PProtein comparedProtein : allprotein) 
            {
                //  PProtein comparedProtein = allprotein.get(allprotein.indexOf(mainprotein));
            //    PProtein comparedProtein = qdb.getprotein("d1bhga1");

                comparedProteinScopId=comparedProtein.getScopsid();
                int[][] mat32SecondProtein = makematrix32(comparedProtein);
                
               // int[][][] targetGradMats = ProteinGradMats(mat64seconPr);
                
                int[][][] targetGradMats = ProteinGradMats(mat32SecondProtein,dim);
                

/*
                printmat(patternGradMats[0]);
                System.out.println("");
                printmat(targetGradMats[0]);
                
                
  */              
                
                
              //  int[] resh = generateHungarianMatResult(patternMaph, targetGradMats[0],dim);
               // boolean[] mh = matchPoins(patternGradMats[0], targetGradMats[0], resh, 0.97, 5,dim);
                /*
                 System.out.println("Printing Results horizontal:");
                for (int u = 0; u < resh.length; u++) {
                    System.out.print( resh[u]+" ");
                }
                System.out.println("");
                for (int u = 0; u < resh.length; u++) {
                    System.out.print( mh[u]+" ");
                }
                
                System.out.println("");
                
                
                
                printmat(patternGradMats[1]);
                System.out.println("");
                printmat(targetGradMats[1]);
                */
            //    int[] resv = generateHungarianMatResult(patternMapv, targetGradMats[1],dim);
             //   boolean[] mv = matchPoins(patternGradMats[1], targetGradMats[1], resv, 0.98, 7,dim);
                /*
                System.out.println("Printing Results vertical:");
                for (int u = 0; u < resv.length; u++) {
                System.out.print( resv[u]+" ");
                }
                 System.out.println("");
 
                for (int u = 0; u < resv.length; u++) {
                System.out.print( mv[u]+" ");
                }
                 System.out.println("");
                   printmat(patternGradMats[2]);
                System.out.println("");
                printmat(targetGradMats[2]);
                */
                
                int[] resd = generateHungarianMatResult(patternMapd, targetGradMats[2],dim);
                boolean[] md = matchPoins(patternGradMats[2], targetGradMats[2], resd, 0.97, 5,dim);
                /*
                System.out.println("Printing Results diagonal:");
                for (int u = 0; u < resd.length; u++) {
                System.out.print( resd[u]+" ");
                }
                System.out.println("");
                for (int u = 0; u < resd.length; u++) {
                System.out.print( md[u]+" ");
                }
                System.out.println("");
                
                //printmat(patternGradMats[3]);
                System.out.println("");
                //printmat(targetGradMats[3]);
                */
                int[] resad = generateHungarianMatResult(patternMapad, targetGradMats[3],dim);
                boolean[] mad = matchPoins(patternGradMats[3], targetGradMats[3], resad, 0.97, 5,dim);
                
                /*
                System.out.println("Printing Results antidiagonal:");
                for (int u = 0; u < resad.length; u++) {
                System.out.print( resad[u]+" ");
                } 
                 System.out.println("");

                 for (int u = 0; u < resad.length; u++) {
                System.out.print( mad[u]+" ");
                } 
                 System.out.println("");
                 */
                
                  //public static boolean[] matchPoins(int[][] mat1,int[][] mat2,int[]res, double edgeMatchThreshold,int countThreshold)

                ////
                //System.out.println("");
             /*
                double msh = calcRms(resh, mh);
                System.out.println("msh:\t" + msh);
                double msv = calcRms(resv, mv);
                System.out.println("msv:\t" + msv);
               
              */
                double msd = calcRms(resd, md);
               // System.out.println("msd:\t" + msd);
                double msad = calcRms(resad, mad);
              //  System.out.println("msad:\t" + msad);

                double rmsd=Math.sqrt(msad+msd);
                //write code here

                // System.out.println(mainprotein.getScopsid()+"\t"+comparedProtein.getScopsid()+"\t:"+rmsd);

                //////

                //double rms_run_len = Math.sqrt((double) ((double) ss_run_len / (double) numberOfRuns));

                int rmsd10=(int)(rmsd*10); 
                String outstring = new String(mainProteinScopId + "\t" + comparedProteinScopId + "\t"+rmsd10 );
                
                System.out.println(outstring);
                
                String newstr;
                
                String current;
                
                current = d_hashsmap.get(rmsd10);
                if (current != null) {
                d_hashsmap.remove(rmsd10);
                newstr = current + ":" + outstring;
                } else {
                newstr = outstring;
                }
                d_hashsmap.put(rmsd10, newstr);
                
               
    
             //   Date endtime1 = new Date();
            //System.out.println("begin at: " + begin.toString() + "\t" + "end at:" + endtime1.toString());
                 
                ///////compare with target ends here//////////////////////       
            
    }
            
           Map<Integer, String> d_treemap = new TreeMap<Integer, String>(d_hashsmap);
            for (Map.Entry en : d_treemap.entrySet()) {
                StringTokenizer strTok = new StringTokenizer(en.getValue().toString(), ":");
                while (strTok.hasMoreTokens()) {
                    String tok = strTok.nextToken().toString();
                    System.out.println(tok);
                    dpw.write(tok);
                    dpw.flush();
                    dpw.println();
                    dpw.flush();
                }
                dpw.flush();
            }
            Date endtime = new Date();
            System.out.println("begin at: " + begin.toString() + "\t" + "end at:" + endtime.toString());
           ////
           // break;
        }
    }

    public static boolean[] matchPoins(int[][] mat1, int[][] mat2, int[] res, double edgeMatchThreshold, int countThreshold,int dim) {

        boolean[] matches = new boolean[dim];
        for (int i = 0; i < dim; i++) {
            int count = 0;
            for (int j = 0; j < dim; j++) {
                int max = Math.max(mat1[i][j], mat2[res[i]][res[j]]);
                int min = Math.min(mat1[i][j],mat2[res[i]][res[j]]);
                if (max == 0 && min == 0) {
                    count++;
                } else if (min == 0 && max <= 1) {
                    count++;
                } else if (min != 0 && ((double) min / (double) max) > edgeMatchThreshold) {
                    count++;
                }
            }
            if (count > countThreshold) {
                matches[i] = true;
            } else {
                matches[i] = false;
            }
        }
        return matches;
       }

    public static int[] generateHungarianMatResult(Map<Integer, String> hm, int[][] matSeconPr,int dim) {

        double[][] hungarianMat = new double[dim][dim];
        for (int k = 0; k < dim; k++) {
            for (int j = 0; j < dim; j++) {
                hungarianMat[k][j] = 0;
            }
        }

        double highest = 0;
        for (int k = 0; k < dim; k++) {
            for (int j = 0; j < dim; j++) {
                int t_edge = matSeconPr[k][j];
                int l ;
                int r;
                if(t_edge<100){
                l= (int) Math.round((double) t_edge * 0.98);
                r = (int) Math.round((double) t_edge * 1.02);
                }else{
                l= (int) Math.round((double) t_edge * 0.96);
                r = (int) Math.round((double) t_edge * 1.04);
                }
                
                for (int p_edge = l; p_edge <= r; p_edge++) {
                    String values = hm.get(p_edge);
                    if (values == null) {
                        continue;
                    }
                    StringTokenizer strTok = new StringTokenizer(values.toString(), ":");
                    while (strTok.hasMoreTokens()) {
                        int tok = Integer.parseInt(strTok.nextToken());
                        int mx = Math.max(t_edge, p_edge);
                        int mn = Math.min(p_edge, t_edge);
                        double incval;
                        if (mx > 0) {
                            incval = (double) ((double) Math.min(p_edge, t_edge) / (double) Math.max(t_edge, p_edge));
                        } else {
                            incval = 1;
                        }
                        hungarianMat[tok][j] = hungarianMat[tok][j] + ((double) incval) * Math.pow(1.02, -Math.abs(j - tok));
                        hungarianMat[tok][k] = hungarianMat[tok][k] + ((double) incval) * Math.pow(1.02, -Math.abs(k - tok));

                        highest = Math.max(highest, Math.max(hungarianMat[tok][j], hungarianMat[tok][k]));
                    }
                }
            }
//                                printmat(hungarianMat);
        }

        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                hungarianMat[i][j] = highest - hungarianMat[i][j];
            }
        }

        HungarianAlgoritm ha = new HungarianAlgoritm(hungarianMat);
        ha.execute();
        int[] res = ha.execute();

        return res;
    }

    public static int[][][] ProteinGradMats(int[][] proteinMat,int dim) {

        int[][][] hvdadMats = new int[4][dim][dim];

        int[][] hf = {{-1, -1, -1}, {0, 0, 0}, {1, 1, 1}};
        int[][] vf = {{-1, 0, 1}, {-1, 0, 1}, {-1, 0, 1}};
        int[][] df = {{0, 1, 1}, {-1, 0, 1}, {-1, -1, 0}};
        int[][] adf = {{-1, -1, 0}, {-1, 0, 1}, {0, 1, 1}};


        int[][] extProteinMat = new int[dim+2][dim+2];

        for (int i = 0; i < dim+2; i++) {
            for (int j = 0; j < dim+2; j++) {
                if (i == 0 || j == 0 || i == dim+1 || j == dim+1) {
                    extProteinMat[i][j] = 0;
                } else {
                    extProteinMat[i][j] = proteinMat[i - 1][j - 1];
                }
            }
        }

        int[] max=new int[4];
        int[] min=new int[4];
        for (int i = 0; i < 4; i++) {
            max[i]=0;
            min[i]=255;
        }
        
        for (int i = 1; i < dim+1; i++) {
            for (int j = 1; j < dim+1; j++) {

                hvdadMats[0][i - 1][j - 1] = 0;
                hvdadMats[1][i - 1][j - 1] = 0;
                hvdadMats[2][i - 1][j - 1] = 0;
                hvdadMats[3][i - 1][j - 1] = 0;
                for (int k = 0; k < 3; k++) {
                    for (int l = 0; l < 3; l++) {
                        hvdadMats[0][i - 1][j - 1] += hf[k][l] * extProteinMat[i - 1 + k][j - 1 + k];
                        hvdadMats[1][i - 1][j - 1] += vf[k][l] * extProteinMat[i - 1 + k][j - 1 + k];
                        hvdadMats[2][i - 1][j - 1] += df[k][l] * extProteinMat[i - 1 + k][j - 1 + k];
                        hvdadMats[3][i - 1][j - 1] += adf[k][l] * extProteinMat[i - 1 + k][j - 1 + k];
                    }
                }

                for (int k = 0; k < 4; k++) {
                    max[k]=Math.max(max[k], hvdadMats[k][i-1][j-1]);
                    min[k]=Math.min(min[k], hvdadMats[k][i-1][j-1]);
                }
                
            }
        }
        
        
        for (int i = 0; i < 4; i++) {
            int l=min[i],u=max[i];
            if(u==l)continue;
            for (int j = 0; j < dim; j++) {
                for (int k = 0; k < dim; k++) {
                    hvdadMats[i][j][k]=(int)(((hvdadMats[i][j][k]-l)*255)/(u-l));
                }
            }
        }
        
        
        
        return hvdadMats;
    }
}
