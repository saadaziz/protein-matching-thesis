/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author Gun2sh
 */
public class masasw {

    /**
     * @param args the command line arguments
     */
    static int w = 5;
    static int matDim = 32;

    public static int calcAbsoluteIndex(int i, int j) {
        int temp = i - w + j;
        return (temp < 0) ? 0 : ((temp > (matDim - 1)) ? (matDim - 1) : temp);

    }

    public static int alignRows(int A[], int B[]) {
        int threshhold = 10;
        int Last[] = new int[2 * w + 1];
        
        for (int i = 0; i < Last.length; i++) {
            Last[i] = 0;
        }
        int Current[] = new int[2 * w + 1];
        for (int i = 0; i < A.length; i++) {
            
            for (int j = 0; j < (w * 2 + 1); j++) {
                int qindex = calcAbsoluteIndex(i, j);
               // System.out.println("qIndex: "+qindex);
                Current[j] = (Math.abs(A[i] - B[qindex]) < threshhold) ? Last[j] + 1
                        : Math.max(Current[j ], Last[j ]);

            }
            Last = Current;
        }
        int temp = -10000;
        for (int i = 0; i < Current.length; i++) {
            temp = Math.max(temp, Current[i]);
        }
        return temp;
    }

    public static double alignMatrices(int A[][], int B[][]) {
        int window = 8;
        int lasMatched = 0, totalSum = 0;
        for (int i = 0; i < A.length; i++) {
            int maxScore = 0, maxScoreIndex = 0;
            for (int j = Math.max(i - window, lasMatched); j < Math.min(i + window,B.length); j++) {
               // System.out.println("J: "+j);
                int score = alignRows(A[i], B[j]);
                if (score > maxScore) {
                    maxScore = score;
                    maxScoreIndex = j;
                }
            }
            totalSum += maxScore;
            lasMatched = maxScoreIndex;
        }
        return totalSum;
    }

    public static int[][] fileTo2dArr(Scanner sc) {
        int retArr[][] = new int[matDim][matDim];
        int index = 0;
        while (sc.hasNext()) {
            StringTokenizer strVals = new StringTokenizer(sc.nextLine(), " ");
            //System.out.println("token size" + strVals.countTokens());
            for (int j = 0; j < strVals.countTokens(); j++) {
                Integer value = Integer.parseInt(strVals.nextToken().trim());
                retArr[index][j] = value;
               // System.out.print(value+" ");
            }
            index++;
           // System.out.println("");
        }
        return retArr;

    }

    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        File inDir = new File("M:\\4-2\\protein\\all sorted for test\\sorted heqep\\comog1norm16level_gray128");
        String searchDir = "M:\\4-2\\protein\\mat\\mat32";
        String outDir = "M:\\4-2\\protein\\mat\\forSortMASASW\\";
        File[] inputFile = inDir.listFiles();
        for (File fl : inputFile) {
            int inputArr[][] = new int[matDim][matDim];
            File searchFile = new File(searchDir + "\\" + fl.getName());
            Scanner sc = new Scanner(searchFile);
            int index = 0;
            inputArr=fileTo2dArr(sc);
            File searchFileDir = new File(searchDir);
            File[] searchFiles = searchFileDir.listFiles();
            File outFile = new File(outDir+fl.getName());
            PrintWriter pw = new PrintWriter(outFile);
            for(File sfile:searchFiles)
            {
                if(!sfile.getName().equals(fl.getName()))
                {
                    Scanner sc2 = new Scanner(sfile);
                    int searchArr[][]=fileTo2dArr(sc2);
                    double alignMat = alignMatrices(inputArr, searchArr);
                    //System.out.println("align mat: "+ sfile.getName()+"   "+alignMat);
                    pw.println(fl.getName().substring(0, 7) +"\t"+sfile.getName().substring(0, 7) +"\t"+alignMat);
                    pw.flush();
                }
            }
            
            System.out.println("`File "+fl.getName());
            pw.checkError();
            pw.close();
           // break;
            //System.out.println(index);
        }
    }
}
