/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import db.PProtein;
import db.PTree;
import db.insertDB;
import db.queryDB;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author rezaul karim
 */
public class maketree {

    /**
     * @param args the command line arguments
     */
    public static int no = 0;

    public double hist_diff(PProtein p1, PProtein p2) {
        double diff = 0;
        List<String> hist1 = Arrays.asList(p1.getHistogram128().split(" "));
        List<String> hist2 =  Arrays.asList(p2.getHistogram128().split(" "));
        for (int i = 0; i < hist1.size(); i++) {
            //System.out.println(hist1.get(i));
            diff += (Math.abs(Integer.parseInt(hist1.get(i))) - Integer.parseInt(hist2.get(i))
                    / (Integer.parseInt(hist1.get(i)) + Integer.parseInt(hist2.get(i)) + 1));

        }
        //System.out.println("diff p1"+p1.getScopsid()+"p2"+p2.getScopsid()+"  "+diff);
        return diff;
    }

    public PTree insertRoot(PProtein p1) {
        insertDB insertd = new insertDB();
        PTree pt = new PTree();
        pt.setPid(p1);
        //pt.setName("R" + no++);
        pt.setChildCount(2);
        return insertd.insertTree(pt);
    }

    public static void main(String[] args) {
//        maketree mtree = new maketree();
//        queryDB qdb = new queryDB();
//        insertDB insertd = new insertDB();
//
//        List<PProtein> proteinarr = qdb.getproteins();
//        for (PProtein protein : proteinarr) {
//            Comparator<Double> comparehist = new Comparator<Double>() {
//
//                @Override
//                public int  compare(Double o1, Double o2) {
//                    return o1.compareTo(o2);
//                }
//            };
//            SortedMap histdiff = new TreeMap(comparehist);
//
//            List<PTree> ptreearr = qdb.gettree();
//            PTree root = new PTree();
//            if (ptreearr.isEmpty()) {
//                root = mtree.insertRoot(protein);
//                PTree pt = new PTree();
//                pt.setPid(protein);
//                /pt.setName(protein.getScopsid());
//                pt.setParentid(root);
//                pt.setChildCount(0);
//                insertd.insertTree(pt);
//            } else if (ptreearr.size() == 2) {
//                PTree pt = new PTree();
//                pt.setPid(protein);
//                pt.setName(protein.getScopsid());
//                pt.setParentid(qdb.getTreenodeByName("R0"));
//                pt.setChildCount(0);
//                insertd.insertTree(pt);
//            } else {
//                int i = 0;
//                PProtein parent = new PProtein();
//                for (PTree ptre : ptreearr) {
//                    if (ptre.getName() != "R0") {
//                        histdiff.put(mtree.hist_diff(ptre.getPid(), protein), ptre);
//                    }
//                    System.out.println("first key "+histdiff.firstKey());
//                //break;
//                }
//                PTree ptr1 = new PTree();
//                ptr1.setParentid((PTree)histdiff.get(histdiff.lastKey()));
//                ptr1.setName(protein.getScopsid());
//                ptr1.setPid(protein);
//                insertd.insertTree(ptr1);
//                PTree oldnode= (PTree)histdiff.get(histdiff.lastKey());
//                oldnode.setChildCount(oldnode.getChildCount()+1);
//                insertd.insertTree(oldnode);
//            }

       // }
    }
}
