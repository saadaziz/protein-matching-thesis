/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Gun2sh
 */
public class myMap extends HashMap<Integer, List<String>> {
    public void put(int key, String number) {
        List<String> current = get(key);
        if (current == null) {
            current = new ArrayList<String>();
            super.put(key, current);
        }
        current.add(number);
    }
}