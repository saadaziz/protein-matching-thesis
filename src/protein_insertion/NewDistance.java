package protein_insertion;

import db.PCamat;
import db.PProtein;
import db.queryDB;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewDistance {

    public static void printhm(HashMap hm) {
        //TreeMap tmp = new TreeMap(hm);
        Set set = hm.entrySet();
        // Get an iterator
        Iterator it = set.iterator();
        // Display elements
        while (it.hasNext()) {
            Map.Entry me = (Map.Entry) it.next();
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
        }
    }

    public static void printmat(int[][] mat) {
        System.out.println("Printing");

        for (int k = 0; k < 64; k++) {
            for (int j = 0; j < 64; j++) {
                System.out.print(mat[k][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] makematrix64(PProtein mainprotein) {
        queryDB qdb = new queryDB();

        List<PCamat> lstcamat = qdb.getallCamat64(mainprotein);
        int[][] mat64mainpr = new int[64][64];
        ///System.out.println("Protein matrix for: " + mainprotein.getPid());
        int i = 0;
        int rownum = 0;
        for (PCamat cmtrows : lstcamat) {
            if (rownum != cmtrows.getRowNo()) {
                i = 0;
            }
            rownum = cmtrows.getRowNo();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol1();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol2();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol3();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol4();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol5();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol6();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol7();
            mat64mainpr[cmtrows.getRowNo()][i++] = cmtrows.getCol8();
        }
        return mat64mainpr;
    }
//        double[][] hungtest = {{10,19,8,15},
//        {10 ,18,7,17},
//        {13,16,9,14},
//        {12,19,8,18},
//        {14,17,10,19}};
//        HungarianAlgoritm ha = new HungarianAlgoritm(hungtest);
//        int[] res = ha.execute();
//        for(int u=0;u<res.length;u++)
//            System.out.println(res[u]);

    public static void main(String[] args) {


        int dimp = 64;
        queryDB qdb = new queryDB();


        int d=0;
        int matchpoints = 0;
        int t_edge = 0;
        int window = 0;
        double dwindow = 0.0;
        int p_edge = 0;
        int l, r;
        double incval = 0;

        double highest = 0;
        int mindist = 63 * 32;
        String s1 = null;
        String s2 = null;
        String outstring = null;
        int maxmatch = 0;
        
        int i1, i2;
        double mn, mx, fact;


        double distance = 0;
        int fmps = 0;
        double ratio = 1;

        int matchEdgePerNode = 0;
        
        double rmsMatchEdgePerNode = 0;
        double rmsMisMatchEdgePerNode = 0;
        int max, min;
        double approxMatchPerNode;

        String outdir="E:\\Thesis\\codes\\protein_insertion\\output\\";
            

        /*
             File badf = new File(outdir+"badlist_rmsmatch.txt");
            PrintWriter badpw = null;
            try {
            badpw= new PrintWriter(badf);
            } catch (FileNotFoundException ex) {
            System.out.println("BAD OUTPUT FILE ERROR");
            }
       
         
         */

        List<PProtein> allprotein = qdb.getproteins();

        myMap hm=null;
            

        int ok=0;
        int nok=0;
      //  for (PProtein mainprotein : allprotein) {

        {
            PProtein mainprotein=qdb.getprotein("d1bhga1");
     
            //_
            /*
            if (mainprotein.getPid() < 7 || mainprotein.getPid() >10 ) {
            continue;
            }
             */

//            System.out.println("pattern" + "\t" + mainprotein.getScopsid().toString());


            hm=null;
            hm = new myMap();

            int[][] mat64mainpr = makematrix64(mainprotein);

            double[][] hungarianMat = new double[64][64];
            for (int k = 0; k < 64; k++) {
                for (int j = 0; j < 64; j++) {
                    hungarianMat[k][j] = 0;
                }
            }
            for (int k = 0; k < 64; k++) {
                for (int j = k + 1; j < 64; j++) {

                    hm.put(mat64mainpr[k][j], new String(k + ":" + j));

                }
            }
            
            

            //printhm(hm);
            highest = 0;
            s1 = null;
            s2 = null;
            maxmatch = 0;
           fmps = 0;


            matchEdgePerNode = 0;
            rmsMatchEdgePerNode = 0;
           

            Map<Integer, String> d_hashsmap = new HashMap<Integer, String>();
             Map<Integer, String> rmsmatch_hashsmap = new HashMap<Integer, String>();
           
           
           // Map<Integer, String> fmps_hashsmap = new HashMap<Integer, String>();
            
            
            
            
            File df = new File(outdir+"distance\\" + mainprotein.getScopsid() + ".txt");
            PrintWriter dpw = null;
            try {
            dpw= new PrintWriter(df);
            } catch (FileNotFoundException ex) {
            System.out.println("OUTPUT FILE ERROR");
            }
            
            
           
            
            
            
            
            
            
            /*
            File fmpsf = new File(outdir+"fmps\\" + mainprotein.getScopsid() + ".txt");
            PrintWriter fmpspw = null;
            try {
            fmpspw = new PrintWriter(fmpsf);
            } catch (FileNotFoundException ex) {
            System.out.println("OUTPUT FILE ERROR");
            }
            
            */
            
            
            File rmsmatchf = new File(outdir+"rmsm\\" + mainprotein.getScopsid() + ".txt");
            PrintWriter rmsmatchpw = null;
            try {
            rmsmatchpw = new PrintWriter(rmsmatchf);
            } catch (FileNotFoundException ex) {
            System.out.println("OUTPUT FILE ERROR");
            }
            
            
            ////////////////////output files and printwriters above/////////////////////
            
             

            /// int comparelimit=30;
            d=0;
           for (PProtein comparedProtein : allprotein) {

            
                 
                
               //  { PProtein comparedProtein = allprotein.get(allprotein.indexOf(mainprotein));

                //  PProtein comparedProtein = qdb.getprotein("d2b0ta");
                ////////this if limits number of compare proteins/////////
                //  if(--comparelimit<1){break;}

                //   if (comparedProtein.getPid() != mainprotein.getPid()) {continue;}

                int[][] mat64seconPr = makematrix64(comparedProtein);

                d=0;
          
                matchpoints = 0;
                t_edge = 0;
                window = 0;
                dwindow = 0.0;
                p_edge = 0;
                incval = 0;

                matchEdgePerNode = 0;
                rmsMatchEdgePerNode = 0;
                

                for (int k = 0; k < 64; k++) {
                    for (int j = k + 1; j < 64; j++) {

                        t_edge = mat64seconPr[k][j];
                        l = (int) Math.round((double) t_edge * 0.97);
                        r = (int) Math.round((double) t_edge * 1.03);

                        for (p_edge = l; p_edge <= r; p_edge++) {

                            List<String> values = (hm.get(p_edge) != null)
                                    ? hm.get(p_edge) : new LinkedList<String>();


                            //   System.out.println("Printing values of hm map");
                            for (String val : values) {


                                // System.out.println(val+"\t"+k+"\t"+j);
                                matchpoints++;


                                StringTokenizer strTok = new StringTokenizer(val, ":");
                                //System.out.println(val);
                                i1 = Integer.parseInt(strTok.nextToken());
                                i2 = Integer.parseInt(strTok.nextToken());
                                mx = Math.max(t_edge, p_edge);
                                mn = Math.min(p_edge, t_edge);


                                if (mx > 0) {
                                    incval = (double) ((double) Math.min(p_edge, t_edge) / (double) Math.max(t_edge, p_edge));


                                    hungarianMat[i1][j] = hungarianMat[i1][j] + ((double) incval) * Math.pow(1.023, -Math.abs(j - i1));//bipartite
                                    hungarianMat[i2][j] = hungarianMat[i2][j] + ((double) incval) * Math.pow(1.023, -Math.abs(j - i2));
                                    hungarianMat[i1][k] = hungarianMat[i1][k] + ((double) incval) * Math.pow(1.023, -Math.abs(k - i1));
                                    hungarianMat[i2][k] = hungarianMat[i2][k] + ((double) incval) * Math.pow(1.023, -Math.abs(k - i2));

                                    highest = Math.max(highest, Math.max(Math.max(hungarianMat[i1][j], hungarianMat[i2][j]),
                                            Math.max(hungarianMat[i1][k], hungarianMat[i2][k])));
                                }
                            }
//                                printmat(hungarianMat);
                        }
                    }
                }



                for (int i = 0; i < dimp; i++) {
                    for (int j = 0; j < dimp; j++) {
                        hungarianMat[i][j] = highest - hungarianMat[i][j];
                    }
                }
                HungarianAlgoritm ha = new HungarianAlgoritm(hungarianMat);
                ha.execute();
                int[] res = ha.execute();

                /*
                System.out.println("Printing Results:");
                for (int u = 0; u < res.length; u++) {
                System.out.println(u + ": " + res[u]);
                }
                 */


                distance = 0;
                fmps = 0;
                ratio = 1;
                rmsMatchEdgePerNode = 0;
                
                for (int u = 0; u < res.length; u++) {

                    matchEdgePerNode = 0;
                    for (int i = 0; i < dimp; i++) {
                        max = Math.max(mat64mainpr[u][i], mat64seconPr[res[u]][res[i]]);
                        min = Math.min(mat64mainpr[u][i], mat64seconPr[res[u]][res[i]]);

                        ratio = (double) ((double) min / (double) max);

                        if (max > 0) {
                            distance = distance + ratio;
                        }

                        if (ratio > 0.995) {
                            fmps++;
                        }

                        if (ratio > 0.99) {
                            matchEdgePerNode++;
                        }

                       

                    }

                    rmsMatchEdgePerNode += (double) (matchEdgePerNode * matchEdgePerNode);
                }




                rmsMatchEdgePerNode = (double)Math.sqrt(rmsMatchEdgePerNode/64);
                d = (63 * 32 - (int) distance);
                int rmsm1000 = (int) (rmsMatchEdgePerNode * 1000);
                int avgfmps = (int)(((double)fmps/64)*1000);
                
                outstring = new String(mainprotein.getScopsid() + "\t" + comparedProtein.getScopsid().toString() + "   \t"
                        + d + "   \t" + avgfmps + "   \t"
                         + rmsm1000 );

                     System.out.println(outstring);
               /*
                     
                     if(d<50 && rmsm1000>2500)
                    ok++;
                else
                {
                     nok++;
                    System.out.println(outstring);
                    badpw.println(outstring);
                    badpw.flush();
                    }
                
                */

                     
                     String newstr;
                String current;

                current = d_hashsmap.get(d);
                if (current != null) {
                    d_hashsmap.remove(d);
                    newstr = current + ":" + outstring;
                } else {
                    newstr = outstring;
                }
                d_hashsmap.put(d, newstr);
          
                 
                 
                 
                
                
                 current = rmsmatch_hashsmap.get(rmsm1000);
                if (current != null) {
                    rmsmatch_hashsmap.remove(rmsm1000);
                    newstr = current + ":" + outstring;
                } else {
                    newstr = outstring;
                }
                rmsmatch_hashsmap.put(rmsm1000, newstr);
           
                 
                 
                 
                 
                 
                 
                 
                 
                 }

            
            
            {
            Map<Integer, String> d_treemap = new TreeMap<Integer, String>(d_hashsmap);
            Map<Integer, String> rmsmatch_treemap = new TreeMap<Integer, String>(rmsmatch_hashsmap);
           
            
            //////////////////writing to file sorted as distance///////////////////////////  
            for (Map.Entry en : d_treemap.entrySet()) {
                StringTokenizer strTok = new StringTokenizer(en.getValue().toString(), ":");
                while (strTok.hasMoreTokens()) {
                    String tok = strTok.nextToken().toString();

                       
//                        System.out.println(tok);

                               dpw.write(tok);
                             dpw.flush();
                           dpw.println();
                         dpw.flush();


                }


                  dpw.flush();
            }

             dpw.flush();  
             
             
             
             dpw.checkError();
             dpw.close();

             
             
             
             
            for (Map.Entry en : rmsmatch_treemap.entrySet()) {
                StringTokenizer strTok = new StringTokenizer(en.getValue().toString(), ":");
                while (strTok.hasMoreTokens()) {
                    String tok = strTok.nextToken().toString();

                       
//                        System.out.println(tok);

                    
                               rmsmatchpw.write(tok);
                             rmsmatchpw.flush();
                           rmsmatchpw.println();
                         rmsmatchpw.flush();


                }


                  rmsmatchpw.flush();
            }

             rmsmatchpw.flush();  
             
               
             
             
             rmsmatchpw.checkError();
             rmsmatchpw.close();

             
             
             
             
             
             
             
             
             
             
             
        }



                 
        }
       
     //   badpw.close();
      //  System.out.println("OK:"+"\t"+ok+"\t"+"not ok:"+"\t"+nok);
    }
}