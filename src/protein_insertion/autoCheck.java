/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import db.PDetails;
import db.PProtein;
import db.insertDB;
import db.queryDB;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeMap;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Gun2sh
 */
public class autoCheck {

    private static boolean checkMatch(String fold1, String fold2) {
        fold1=fold1.replaceAll("[0-9]","").replaceAll(":", "");
        fold2=fold2.replaceAll("[0-9]","").replaceAll(":", "");
        System.out.println("Fold "+fold1);
        //fold2=fold1.replaceAll("[0-9]","").replaceAll(":","");
        return (fold1 == null ? fold2 == null : fold1.equals(fold2));
    }

    /**
     * @param args the command line arguments
     */
    public PDetails fparsescop(String sunid, String scopid) {
        try {
            Document doc = Jsoup.connect("http://scop.berkeley.edu/sunid=" + sunid).timeout(10 * 1000).get();
            Elements descboxs = doc.getElementsByClass("descbox");
            int i = 0;
            String Class1 = "", fold = "", superf = "", family = "", protein = "", species = "";
            for (Element es : descboxs) {
                String text = es.select("a[href]").first().text().trim();
                if (i == 0) {
                    Class1 = text.substring(2, text.length()).replace(":", " ").trim();
                } else if (i == 1) {
                    fold = text.substring(4, text.length()).replace(":", " ").trim();
                } else if (i == 2) {
                    superf = text.substring(6, text.length()).replace(":", " ").trim();
                } else if (i == 3) {
                    family = text.substring(8, text.length()).replace(":", " ").trim();
                } else if (i == 4) {
                    protein = text.trim();
                } else if (i == 5) {
                    species = text.trim();
                } else {
                    continue;
                }
                
                i++;
            }
            System.out.println(Class1 + "," + fold + "," + superf + "," + family + "," + protein + "," + species);
            if (Class1 != "") {
                queryDB qdb = new queryDB();
                insertDB idb = new insertDB();
                PProtein pr = qdb.getprotein(scopid);
                PDetails pdetails = new PDetails();
                pdetails.setClass1(Class1);
                pdetails.setFold(fold);
                pdetails.setFamily(family);
                pdetails.setSuperfamily(superf);
                pdetails.setProtein(protein);
                pdetails.setSpecies(species);
                pdetails.setPid(pr);
                pdetails.setSunid(sunid);
                if (Class1 != "") {
                    pdetails = idb.insertDetails(pdetails);
                    return pdetails;
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            fparsescop(sunid, scopid);
        }
        return null;
    }
    
    public PDetails getnewdetails(String scid) throws IOException {
        //System.out.println("On insert");
        File fl = new File("M:\\4-2\\protein\\sunid map all\\allsunid.txt");
        Scanner sc = new Scanner(fl);
        PDetails pdet = new PDetails();
        queryDB qdb = new queryDB();
        while (sc.hasNext()) {
            String line = sc.nextLine();
            StringTokenizer strTok = new StringTokenizer(line, "\n\r");
            int dim = strTok.countTokens();
            for (int j = 0; j < dim; j++) {
                StringTokenizer strtokspace = new StringTokenizer(strTok.nextToken(), "\t");
                if (strtokspace.countTokens() != 0) {
                    String scopid = strtokspace.nextToken().trim();
                    String sunid = strtokspace.nextToken().trim();
                    if (scid.equals(scopid)) {
                        if (!qdb.ifDetailsExists(sunid)) {
                            pdet = fparsescop(sunid, scopid);
                        }
                    }
                    
                }
            }
        }
        return pdet;
    }
    
    public static void main(String[] args) throws IOException {
        ArrayList<String> args1 = new ArrayList<String>();
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        final DefaultCategoryDataset datasetFold = new DefaultCategoryDataset();
        int kl = 0;
        //ArrayList<String> args2 =new ArrayList<String>();
        // String inDirName = "M:\\4-2\\protein\\mat\\sortedMASASW";
        String inDirName = "M:\\4-2\\protein\\all sorted for test\\sorted heqep";
        //String inDirName = "M:\\4-2\\protein\\all sorted for test\\sorted heqep\\check";
        File inDir = new File(inDirName);
        File[] inputFile2 = inDir.listFiles();
        for (File fl2 : inputFile2) {
            args1.add(fl2.getName());
            File[] inputFile = fl2.listFiles();
            int flcount=inputFile.length;
            Map<Integer,Integer> classMatchMap = new TreeMap<Integer, Integer>();
            Map<Integer,Integer> foldMatchMap = new TreeMap<Integer, Integer>();
            for (File fl : inputFile) {
                //File fl = new File("M:\\4-2\\protein\\all sorted for test\\sorted heqep\\comog1norm16level_gray128\\d4jcee_.ent");
                autoCheck thisob = new autoCheck();
                queryDB qdb = new queryDB();
                insertDB idb = new insertDB();
                Scanner sc = new Scanner(fl);
                int distance = 0;
                String current = "";
                int totalrun = 0;
                ArrayList<String> targetlist = new ArrayList<String>();
                ArrayList<Integer> distList = new ArrayList<Integer>();
                while (sc.hasNext()) {
                    String line = sc.nextLine();
                    StringTokenizer strTok = new StringTokenizer(line, "\n\r");
                    int dim = strTok.countTokens();
                    for (int j = 0; j < dim; j++) {
                        StringTokenizer strtokspace = new StringTokenizer(strTok.nextToken(), " ");
                        if (strtokspace.countTokens() != 0) {
                            current = strtokspace.nextToken().trim().substring(0, 7);
                            String target = strtokspace.nextToken().trim().substring(0, 7);
                            distance = Integer.parseInt(strtokspace.nextToken().trim());
                            //System.out.println("current:"+current+" target:"+target+" distace:"+distance);
                            targetlist.add(target);
                            distList.add(distance);
                            
                        }
                    }
                    totalrun++;
                    if ((totalrun) > 99) {
                        break;
                    }
                    //else if(totalrun>10)
                }
                int cnt = 0;
                int match = 0,matchFold=0, total = 1;
                PProtein main = qdb.getprotein(current);
                PDetails maindetails = new PDetails();
                if (qdb.ifDetailsByPid(main)) {
                    maindetails = qdb.getDetailsByPid(main);
                } else {
                    maindetails = thisob.getnewdetails(current);
                }
                
                for (String target : targetlist) {
                    PProtein targetpr = new PProtein();
                    PDetails targetdetails = new PDetails();
                    if (!qdb.ifProteinexists(target)) {
                        //System.out.println("Target1: "+target);
                        targetpr.setScopsid(target);
                        targetpr.setPdbid(target.substring(1, 5));
                        targetpr.setDataset(0);
                        targetpr = idb.insertProteinRet(targetpr);
                        targetdetails = thisob.getnewdetails(target);
                    } else {
                        
                        targetpr = qdb.getprotein(target);
                        /*PEdgelist edge = new PEdgelist();
                        edge.setDistance((double)(distList.get(cnt)));
                        edge.setPid1(main);
                        edge.setPid2(targetpr);
                        edge.setMethod("comog1norm32level_gray128");*/
                        //if(main.getPid()!=targetpr.getPid())
                        //    new insertDB().insertEdgelist(edge);
                        // System.out.println("Target2: "+targetpr.getScopsid());
                        if (qdb.ifDetailsByPid(targetpr)) {
                            targetdetails = qdb.getDetailsByPid(targetpr);
                        } else {
                            targetdetails = thisob.getnewdetails(target);
                        }
                    }
                    
                    try {
                        //System.out.print(maindetails.getClass1() + ":::");
                        match = (maindetails.getClass1().equals(targetdetails.getClass1())) ? match + 1 : match;
                        matchFold = (checkMatch(maindetails.getFold(),targetdetails.getFold())) ? matchFold + 1 : matchFold;
                        //System.out.print((maindetails.getClass1().equals(targetdetails.getClass1())) ? " Class matched " : " Class not matched ");
                    /*System.out.print((maindetails.getFold().equals(targetdetails.getFold()))?" Fold matched ":" Fold not matched ");/*
                        System.out.print((maindetails.getSuperfamily().equals(targetdetails.getSuperfamily()))?" Superfamily matched ":" Superfamily not matched ");
                        System.out.print((maindetails.getFamily().equals(targetdetails.getFamily()))?" Family matched ":" Family not matched ");
                        System.out.print((maindetails.getProtein().equals(targetdetails.getProtein()))?" Protein matched ":" Protein not matched ");
                        System.out.print((maindetails.getSpecies().equals(targetdetails.getSpecies()))?" Species matched ":" Species not matched ");
                         */
                        //System.out.println("Match " + match + " Total " + total);
                        if (total % 10 == 0) {
                            /*args1[kl++] = (String.valueOf(match));
                            args1[kl++] = (maindetails.getPid().getScopsid());
                            args1[kl++] = (String.valueOf(total));*/
                            classMatchMap.put(total, (classMatchMap.get(total)==null)?match:classMatchMap.get(total)+match);
                            foldMatchMap.put(total, (foldMatchMap.get(total)==null)?matchFold:foldMatchMap.get(total)+matchFold);
                            args1.add((String.valueOf(match)));
                            args1.add(maindetails.getPid().getScopsid());
                            args1.add(String.valueOf(total));
                        }
                        total++;
                    } catch (NullPointerException ex) {
                        System.out.println("For target:" + target);
                    }
                    
                    cnt++;
                }
                //System.out.println("total match " + match + " total args sent " + total);
                /*String[] tmpargs = new String[args1.size()];
                tmpargs = (String[]) args1.toArray();*/
                //break;                
            }
            for(Integer mt:classMatchMap.keySet())
            {
                dataset.addValue((double)(100*classMatchMap.get(mt)/(mt*flcount)), fl2.getName(),mt);
                datasetFold.addValue((double)(100*foldMatchMap.get(mt)/(mt*flcount)), fl2.getName(),mt);
            }
        }
        
        
        final BarChart3DDemo2 demo = new BarChart3DDemo2("Class",dataset);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
        final BarChart3DDemo2 demo2 = new BarChart3DDemo2("Fold",datasetFold);
        demo2.pack();
        RefineryUtilities.centerFrameOnScreen(demo2);
        demo2.setVisible(true);
        /*String[] sendingArr = new String[args1.size()];
        BarChart3DDemo2.main(args1.toArray(sendingArr));*/
    }
}
