/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package protein_insertion;

import db.PDistancetree;
import db.PProtein;
import db.insertDB;
import db.queryDB;
import java.io.Console;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author Gun2sh
 */
public class edgelist {
    queryDB qdb = new queryDB();
    public static void main(String[] args) {
        edgelist edlst = new edgelist();
        Scanner scan = new Scanner(System.in);
        int input = scan.nextInt(); 
        //PProtein pr = edlst.qdb.getprotein(input);
        List<PProtein> proteinlst = edlst.qdb.getproteins();
        Map<Integer, Integer> treeMap = new TreeMap<Integer, Integer>();
        for(PProtein p : proteinlst)
        {
             List<String> hist1 = Arrays.asList( p.getHistogram128().split(" "));
             Map<Integer, Integer> unsortMap = new HashMap<Integer, Integer>();
            for(PProtein p1 : proteinlst)
            {
                if(p.getPid()==p1.getPid())continue;
                if(p.getPid()<input) continue;
                //System.out.println("Computing for "+p.getScopsid());
                List<String> hist2 = Arrays.asList(  p1.getHistogram128().split(" "));
                int diff =0;
                for(int i =0;i<128;i++)
                {
                    diff+=Math.abs(Integer.parseInt(hist1.get(i))-Integer.parseInt(hist2.get(i)));
                }
                unsortMap.put( diff,p1.getPid());
            }
            treeMap = new TreeMap<Integer, Integer>(unsortMap);
            int ind=0;
            for (Map.Entry entry : treeMap.entrySet()) {
                ind++;
                if(ind==101)break;
			System.out.println(ind+". Current PID = "+p.getPid()+" distance : " +Integer.parseInt( entry.getKey().toString()) + "  PID : "
				+ entry.getValue());
                PDistancetree pdtree = new PDistancetree();
                pdtree.setDistance(Integer.parseInt( entry.getKey().toString()));
                pdtree.setFirstpid(p);
                pdtree.setSecondpid(edlst.qdb.getprotein(Integer.parseInt( entry.getValue().toString())));
                new insertDB().insertdistanceTree(pdtree);
		}
        }
    }
    
}
