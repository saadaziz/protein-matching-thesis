package db;

import db.PProtein;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2014-01-27T01:22:01")
@StaticMetamodel(PEdgelist.class)
public class PEdgelist_ { 

    public static volatile SingularAttribute<PEdgelist, Double> distance;
    public static volatile SingularAttribute<PEdgelist, String> method;
    public static volatile SingularAttribute<PEdgelist, Integer> edgeListid;
    public static volatile SingularAttribute<PEdgelist, PProtein> pid2;
    public static volatile SingularAttribute<PEdgelist, PProtein> pid1;

}