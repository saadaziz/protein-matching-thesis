package db;

import db.PProtein;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2014-01-27T01:22:01")
@StaticMetamodel(PDistancetree.class)
public class PDistancetree_ { 

    public static volatile SingularAttribute<PDistancetree, Integer> distance;
    public static volatile SingularAttribute<PDistancetree, Integer> diffID;
    public static volatile SingularAttribute<PDistancetree, PProtein> firstpid;
    public static volatile SingularAttribute<PDistancetree, PProtein> secondpid;

}