package db;

import db.PProtein;
import db.PTree;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2014-01-27T01:22:01")
@StaticMetamodel(PTree.class)
public class PTree_ { 

    public static volatile SingularAttribute<PTree, Integer> distance;
    public static volatile SingularAttribute<PTree, Integer> childCount;
    public static volatile SingularAttribute<PTree, PTree> parentid;
    public static volatile SingularAttribute<PTree, PProtein> pid;
    public static volatile SingularAttribute<PTree, Integer> tid;
    public static volatile CollectionAttribute<PTree, PTree> pTreeCollection;

}