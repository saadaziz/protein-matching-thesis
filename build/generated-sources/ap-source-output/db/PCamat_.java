package db;

import db.PProtein;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2014-01-27T01:22:01")
@StaticMetamodel(PCamat.class)
public class PCamat_ { 

    public static volatile SingularAttribute<PCamat, Integer> col5;
    public static volatile SingularAttribute<PCamat, Integer> col4;
    public static volatile SingularAttribute<PCamat, Integer> col7;
    public static volatile SingularAttribute<PCamat, Integer> col6;
    public static volatile SingularAttribute<PCamat, Integer> col1;
    public static volatile SingularAttribute<PCamat, Integer> col3;
    public static volatile SingularAttribute<PCamat, Integer> col2;
    public static volatile SingularAttribute<PCamat, PProtein> pid;
    public static volatile SingularAttribute<PCamat, Integer> cid;
    public static volatile SingularAttribute<PCamat, Integer> col8;
    public static volatile SingularAttribute<PCamat, Integer> matDim;
    public static volatile SingularAttribute<PCamat, Integer> rowNo;
    public static volatile SingularAttribute<PCamat, Integer> colNo;

}