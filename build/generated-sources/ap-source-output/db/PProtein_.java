package db;

import db.PDetails;
import db.PEdgelist;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2014-01-27T01:22:01")
@StaticMetamodel(PProtein.class)
public class PProtein_ { 

    public static volatile CollectionAttribute<PProtein, PEdgelist> pEdgelistCollection;
    public static volatile CollectionAttribute<PProtein, PDetails> pDetailsCollection;
    public static volatile SingularAttribute<PProtein, Integer> dataset;
    public static volatile SingularAttribute<PProtein, String> pdbid;
    public static volatile SingularAttribute<PProtein, String> histogram128;
    public static volatile SingularAttribute<PProtein, String> name;
    public static volatile SingularAttribute<PProtein, String> histogram;
    public static volatile SingularAttribute<PProtein, Integer> pid;
    public static volatile SingularAttribute<PProtein, String> scopsid;
    public static volatile CollectionAttribute<PProtein, PEdgelist> pEdgelistCollection1;

}