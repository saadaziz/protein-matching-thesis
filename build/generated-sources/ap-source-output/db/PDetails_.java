package db;

import db.PProtein;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2014-01-27T01:22:01")
@StaticMetamodel(PDetails.class)
public class PDetails_ { 

    public static volatile SingularAttribute<PDetails, String> sunid;
    public static volatile SingularAttribute<PDetails, String> species;
    public static volatile SingularAttribute<PDetails, String> class1;
    public static volatile SingularAttribute<PDetails, String> superfamily;
    public static volatile SingularAttribute<PDetails, String> family;
    public static volatile SingularAttribute<PDetails, String> protein;
    public static volatile SingularAttribute<PDetails, PProtein> pid;
    public static volatile SingularAttribute<PDetails, String> fold;
    public static volatile SingularAttribute<PDetails, Integer> detid;

}