package db;

import db.PProtein;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.2.0.v20110202-r8913", date="2014-01-27T01:22:01")
@StaticMetamodel(PCamat8.class)
public class PCamat8_ { 

    public static volatile SingularAttribute<PCamat8, Integer> col5;
    public static volatile SingularAttribute<PCamat8, Integer> col4;
    public static volatile SingularAttribute<PCamat8, Integer> col7;
    public static volatile SingularAttribute<PCamat8, Integer> col6;
    public static volatile SingularAttribute<PCamat8, Integer> col1;
    public static volatile SingularAttribute<PCamat8, Integer> col3;
    public static volatile SingularAttribute<PCamat8, Integer> col2;
    public static volatile SingularAttribute<PCamat8, PProtein> pid;
    public static volatile SingularAttribute<PCamat8, Integer> rowNo;
    public static volatile SingularAttribute<PCamat8, Integer> col8;
    public static volatile SingularAttribute<PCamat8, Integer> cid;
    public static volatile SingularAttribute<PCamat8, Integer> colNo;

}